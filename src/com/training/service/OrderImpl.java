package com.training.service;

import java.util.List;

import com.training.DAO.DAOImpl;
import com.training.DAO.IDAO;
import com.training.bean.Item;

/**
 * @author kkuhik
 *
 */
public class OrderImpl implements IOrder {

	@Override
	public List<Item> orderedItems(List<Item> list) {
		// TODO Auto-generated method stub
		/*
		 * int index = 0; while (list.size() > index) { index++; }
		 */

		ICustomer customer = new CustomerImpl();
		customer.getAllItems("Starter");
		return list;
	}

	@Override
	public void editOrder(Item item) {
		// TODO Auto-generated method stub
		IDAO ref = new DAOImpl();
		ref.editItem(item);
	}

	@Override
	public void generateBill(List<Item> list) {
		// TODO Auto-generated method stub
		long netAmount = 0;
		int index = 0;
		while (list.size() > index) {
			netAmount += list.get(index).getPrice();
			index++;
		}
		IDAO ref = new DAOImpl();
		ref.updateBill(netAmount);
	}

}
