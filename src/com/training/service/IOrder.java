package com.training.service;

import java.util.List;

import com.training.bean.Item;

/**
 * @author kkuhik
 *
 */
public interface IOrder {
	List<Item> orderedItems(List<Item> list);

	void editOrder(Item item);

	void generateBill(List<Item> list);
}
