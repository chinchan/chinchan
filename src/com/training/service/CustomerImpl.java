package com.training.service;

import java.sql.Connection;
import java.util.List;

import com.training.DAO.DAOImpl;
import com.training.DAO.IDAO;
import com.training.bean.Customer;
import com.training.bean.Item;
//akash
public class CustomerImpl implements ICustomer {
 
	@Override
	public void addCustomer(Customer customer) {
	IDAO refrence=new DAOImpl();
    refrence.createCustomer(customer);
		
		
	}
	@Override
	public boolean validateCustomer(String name, String password) {
		IDAO refrence=new DAOImpl();
		return refrence.isValidCustomer(name, password);
		
		
		
	}
	@Override
	public List<Item> getAllItems(String table) {
		IDAO refrence=new DAOImpl();
		
		return refrence.getAllItems(table);
	

	}

	

}
