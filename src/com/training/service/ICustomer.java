package com.training.service;

import java.util.List;

import com.training.bean.Customer;
import com.training.bean.Item;
//akash
public interface ICustomer {
	void addCustomer(Customer customer);
 
	boolean validateCustomer(String name, String password);
	List<Item> getAllItems(String table);

	
}
