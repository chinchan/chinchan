package com.training.service;

/**
 * @author kkuhik
 *
 */
import java.util.List;

import com.training.bean.Item;

public interface IItem {
	void addItem(Item item);

	void editItem(Item item);

	void deleteItem(Item item);

	void revenue(List<Item> list);
}
