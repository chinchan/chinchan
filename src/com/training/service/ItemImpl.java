package com.training.service;

/**
 * @author kkuhik
 *
 */
import java.util.List;

import com.training.DAO.DAOImpl;
import com.training.DAO.IDAO;
import com.training.bean.Item;

public class ItemImpl implements IItem {

	@Override
	public void addItem(Item item) {
		// TODO Auto-generated method stub
		IDAO ref = new DAOImpl();
		ref.InsertItem(item);
	}

	@Override
	public void editItem(Item item) {
		// TODO Auto-generated method stub
		IDAO ref = new DAOImpl();
		ref.editItem(item);
	}

	@Override
	public void deleteItem(Item item) {
		// TODO Auto-generated method stub
		IDAO ref = new DAOImpl();
		ref.deleteItem(item);
	}

	@Override
	public void revenue(List<Item> list) {
		// TODO Auto-generated method stub
		int index = 0;
		double amount = 0;
		while (list.size() > index) {
			amount += list.get(index).getPrice();
			index++;
		}
		IDAO ref = new DAOImpl();
		// ref.getBill(amount);
	}

}
