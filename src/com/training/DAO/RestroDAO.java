package com.training.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class RestroDAO {
	static Connection con;
	public static Connection openConnection(){
		String driverName="oracle.jdbc.driver.OracleDriver";
		String url="jdbc:oracle:thin:@localhost:1521:xe";
		String username="akash";
		String password="akash";
		try{
			Class.forName(driverName);
			con=DriverManager.getConnection(url, username, password);
		}catch(Exception e){
			e.printStackTrace();
		}
		return con;
	}
	 public  static void closeConnection(){
    	 try {
    		 if(con!=null)
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
     }
}
