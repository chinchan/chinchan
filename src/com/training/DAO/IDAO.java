package com.training.DAO;

import java.util.ArrayList;

import com.training.bean.Customer;
import com.training.bean.Item;

public interface IDAO {
	public void InsertItem(Item item);
	ArrayList<Item> getAllItems(String table);
	public void editItem(Item item);
	public void deleteItem(Item item);
	public void  createCustomer(Customer customer);
	public boolean isValidCustomer(String name,String password);
	public void updateBill(double amount);
	public double getRevenue();
}
