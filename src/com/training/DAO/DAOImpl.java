package com.training.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.training.bean.Customer;
import com.training.bean.Item;

public class DAOImpl implements IDAO {
	
	@Override
	public void InsertItem(Item item) {
		Connection con=RestroDAO.openConnection();
		PreparedStatement ps=null;
		try {
			
			String query="insert into ITEMS values(ITEM_ID.nextval,?,?,?)";
			ps=con.prepareStatement(query);
			ps.setString(1, item.getItemName());
			ps.setDouble(2, item.getPrice());
			ps.setString(3, item.getCategory());
			ps.execute();	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			try {
				if(ps!=null)
				ps.close();
				RestroDAO.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public ArrayList<Item> getAllItems(String category1) {
		ArrayList<Item> itemList=null;
		Item item=null;
		Connection con=RestroDAO.openConnection();
		PreparedStatement ps=null;
		try {
			String query="select * from ITEMS where CATEGORY=?";
			ps=con.prepareStatement(query);
			ps.setString(1,category1);
			ResultSet res=ps.executeQuery();
			itemList=new ArrayList<Item>();
			while(res.next()){
				item=new Item();
				String name=res.getString(2);
				double price=res.getDouble(3);
				String category=res.getString(4);
				item.setItemName(name);
				item.setPrice(price);
				item.setCategory(category);
				itemList.add(item);
			}
			return itemList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			try {
				if(ps!=null)
				ps.close();
				RestroDAO.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}return itemList;
	}

	@Override
	public void editItem(Item item) {
		Connection con=RestroDAO.openConnection();
		PreparedStatement ps=null;
		try{
			String query="update ITEMS set price=? where NAME=?";
			ps=con.prepareStatement(query);
			ps.setDouble(1, item.getPrice());
			ps.setString(2,item.getItemName());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			try {
				if(ps!=null)
				ps.close();
				RestroDAO.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void deleteItem(Item item) {
		Connection con=RestroDAO.openConnection();
		PreparedStatement ps=null;
		try{
			String query="delete from ITEMS where NAME=?";
			ps=con.prepareStatement(query);
			ps.setString(1,item.getItemName());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			try {
				if(ps!=null)
				ps.close();
				RestroDAO.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
   //akash
	@Override
	public void createCustomer(Customer customer) {
		String insertQuery="insert into customer values(custid.nextval,?,?)";
		Connection con=RestroDAO.openConnection();
		PreparedStatement ps=null;
		try {
			ps=con.prepareStatement(insertQuery);
			ps.setString(1,customer.getName() );
			ps.setString(2,customer.getPassword() );
			ps.execute();
			System.out.println("inserted");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				if(ps!=null)
				ps.close();
				RestroDAO.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override

	public boolean isValidCustomer(String name, String password) {
		boolean status=false;
		Connection con=RestroDAO.openConnection();
		PreparedStatement ps=null;
		try {
			ps=con.prepareStatement("select * from customer where name=? and password=?");
			ps.setString(1, name);
			ps.setString(2, password);
			ResultSet rs=ps.executeQuery();
			status = rs.next();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				if(ps!=null)
				ps.close();
				RestroDAO.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(status){
			return true;
		}else{
			return false;
		}

	}

	@Override
	public void updateBill(double amount) {
		Connection con=RestroDAO.openConnection();
		PreparedStatement ps=null;
		try {
			String query="insert into BILL_TABLE values(BILL_ID.nextval,?)";
			ps=con.prepareStatement(query);
				ps.setDouble(1, amount);
			ps.execute();	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			try {
				if(ps!=null)
				ps.close();
				RestroDAO.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public double getRevenue() {
		Connection con=RestroDAO.openConnection();
		PreparedStatement ps=null;
		try {
			String query="select SUM(amount) as REVENUE from BILL_TABLE";
			
			ps=con.prepareStatement(query);	
			ResultSet res=ps.executeQuery();
			double revenue=0;
			while(res.next()){
				revenue=res.getDouble(1);
			}
	return revenue;
		}catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			try {
				if(ps!=null)
				ps.close();
				RestroDAO.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return 0;

}
}
