package com.training.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.training.bean.Customer;
import com.training.bean.Item;
import com.training.service.CustomerImpl;
import com.training.service.ICustomer;

/**
 * Servlet implementation class DessertServlet
 */
@WebServlet("/DessertServlet")
public class DessertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DessertServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ICustomer icust = new CustomerImpl();
		String dessert = request.getParameter("dessert");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
	
		List<Item> desertlist = icust.getAllItems("dessert");
		if (desertlist.size() == 0) {
			out.println("There is no data");
		} else {
			session.setAttribute( "DessertList",desertlist);
		}
		
		RequestDispatcher rd= request.getRequestDispatcher("dessert.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
