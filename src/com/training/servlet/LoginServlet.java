package com.training.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.training.bean.Customer;
import com.training.service.CustomerImpl;
import com.training.service.ICustomer;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/loginServlet")
//akash
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		String name=request.getParameter("name");
		String password=request.getParameter("password");
		if(name.equals("admin")&&password.equals("admin")){
			
			RequestDispatcher rd=request.getRequestDispatcher("adminitem.jsp");
			rd.include(request, response);
			
		}else{
			ICustomer ref=new CustomerImpl();
			boolean customer=ref.validateCustomer(name,password);
			if(customer){
				request.setAttribute("uname", name);
				RequestDispatcher rd=request.getRequestDispatcher("welcome.jsp");
				rd.include(request, response);
			}else{
				out.println("<h3 style='color: white; font-size: 25px'>Please Enter a valid username and Password</h3><br>");
				RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
				rd.include(request, response);
			}
			
		}
	}

}
