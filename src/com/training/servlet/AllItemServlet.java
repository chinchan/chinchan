package com.training.servlet;

/**
 * @author kkuhik
 *
 */
import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.training.bean.Item;
import com.training.service.CustomerImpl;
import com.training.service.ICustomer;

/**
 * Servlet implementation class AllItemServlet
 */
@WebServlet("/allItemServlet")
public class AllItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AllItemServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		ICustomer ref = new CustomerImpl();
		List<Item> items = ref.getAllItems("starter");
		List<Item> items2 = ref.getAllItems("maincourse");
		List<Item> items3 = ref.getAllItems("dessert");
		items.addAll(items2);
		items.addAll(items3);
		System.out.println(items);
		String optionName = request.getParameter("select");
		request.setAttribute("items", items);
		
		/*items.addAll(ref.getAllItems("maincourse"));
		items.addAll(ref.getAllItems("dessert"));*/
		
		if(optionName.equalsIgnoreCase("Add")) {
			RequestDispatcher rd = request.getRequestDispatcher("additem.jsp");
			rd.include(request, response);
		} else if(optionName.equalsIgnoreCase("Edit")) {
			RequestDispatcher rd = request.getRequestDispatcher("edititem.jsp");
			rd.include(request, response);
		} else if(optionName.equalsIgnoreCase("Delete")) {
			RequestDispatcher rd = request.getRequestDispatcher("deleteitem.jsp");
			rd.include(request, response);
		} else if(optionName.equalsIgnoreCase("Revenue")) {
			RequestDispatcher rd = request.getRequestDispatcher("RevenueServlet");
			rd.include(request, response);
		} else
			response.sendRedirect("adminitem.jsp");
		
		/*RequestDispatcher rd = request.getRequestDispatcher("edititem.jsp");
		rd.include(request, response);*/

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
