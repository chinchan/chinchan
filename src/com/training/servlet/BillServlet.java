package com.training.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.training.DAO.DAOImpl;
import com.training.DAO.IDAO;
import com.training.bean.Item;

/**
 * Servlet implementation class BillServlet
 */
@WebServlet("/billServlet")
public class BillServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BillServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		double total=0;
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		ArrayList<Item> items=(ArrayList<Item>) session.getAttribute("listItems");
		System.out.println(items);
		if(items.size()==0)
		{
			out.println("<h1>Items Empty....Please select</h1>");
			RequestDispatcher rd=request.getRequestDispatcher("welcome.jsp");
			rd.include(request, response);
			return;
		}
		else{
			for(Item item:items){
				total+=item.getPrice();
				System.out.println(item);
			}
			System.out.println(total);
			request.setAttribute("total",total);
			IDAO ref=new DAOImpl();
			ref.updateBill(total);
			RequestDispatcher rd=request.getRequestDispatcher("bill.jsp");
			rd.include(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
