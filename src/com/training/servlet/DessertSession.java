package com.training.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.training.bean.Item;

/**
 * Servlet implementation class DessertSession
 */
@WebServlet("/dessertSession")
public class DessertSession extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DessertSession() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		String dessertArray[]=request.getParameterValues("dessert");
		ArrayList<Item> list =(ArrayList<Item>) session.getAttribute("listItems");
		if(list==null){
			list = new ArrayList<>();
			
		}if(dessertArray==null){
			 RequestDispatcher rd= request.getRequestDispatcher("welcome.jsp");
				rd.forward(request, response);}
		else{
		 for (String allitems:dessertArray) {
			String itemArray[] = allitems.split("--");
				Item itemN = new Item();
				itemN.setItemName(itemArray[0]);
				itemN.setPrice(Double.parseDouble(itemArray[1]));
				System.out.println(itemN);
				list.add(itemN);
				System.out.println(list);
			}
		 session.setAttribute("listItems",list);
		
	   RequestDispatcher rd= request.getRequestDispatcher("welcome.jsp");
		rd.forward(request, response);
	}}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
