package com.training.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.training.bean.Item;

/**
 * Servlet implementation class EditServlet
 */
@WebServlet("/editServlet")
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		String finalArray[]=request.getParameterValues("finalOrder");
		ArrayList<Item> list =(ArrayList<Item>) session.getAttribute("listItems");
		ArrayList<Item> newList=new ArrayList<Item>();
		ArrayList<Item> finalList=new ArrayList<Item>();
		if(list==null){
			list = new ArrayList<>();
			
		}
		
		 try{for (String allitems:finalArray) {
			
			String itemArray[] = allitems.split("--");
				Item itemN = new Item();
				itemN.setItemName(itemArray[0]);
				itemN.setPrice(Double.parseDouble(itemArray[1]));
				System.out.println(itemN);
				
				newList.add(itemN);}
		 
		
		 System.out.println(newList);
		 for(Item itemarr:list){
			 for(Item arr:newList){
				 System.out.println(arr.getItemName());
			 if(!(itemarr.getItemName().equals(arr.getItemName()))){
				
				 finalList.add(itemarr);
				 
			 }
		 }
			
		 
		 }
		 System.out.println(finalList);
		 session.setAttribute("listItems",finalList);
	   RequestDispatcher rd= request.getRequestDispatcher("checkout.jsp");
		rd.forward(request, response);
		 }
		 catch(NullPointerException e){
			 RequestDispatcher rd= request.getRequestDispatcher("checkout.jsp");
				rd.forward(request, response);
				return;
		 }
		 }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
