package com.training.servlet;

/**
 * @author kkuhik
 *
 */
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.training.bean.Item;
import com.training.service.IItem;
import com.training.service.ItemImpl;

/**
 * Servlet implementation class AddItemServlet
 */
@WebServlet("/addItemServlet")
public class AddItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddItemServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String itemName = request.getParameter("itemName");
		String category = request.getParameter("category");
		double price;
		Item item = new Item();
		try {
			price = Double.parseDouble(request.getParameter("price"));
			item.setPrice(price);
		} catch (NumberFormatException e) {
			response.sendRedirect("additem.jsp");
		}
		 //Because everything is taken as String
		if(itemName==""||category==""||item.getPrice()<0){
			response.sendRedirect("additem.jsp");
			/*RequestDispatcher rd=request.getRequestDispatcher("additem.jsp");
			rd.include(request, response);*/
		}else{
			item.setItemName(itemName);
		item.setCategory(category);
		
		
		IItem ref = new ItemImpl();
		ref.addItem(item);
		out.println("<h1>Item Added</h1>");
		
		RequestDispatcher rd = request.getRequestDispatcher("adminitem.jsp");
		rd.include(request, response);}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
