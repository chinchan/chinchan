package com.training.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.training.bean.Item;
import com.training.service.CustomerImpl;
import com.training.service.ICustomer;

/**
 * Servlet implementation class miancourseServlet
 */
@WebServlet("/maincourseServlet")
public class MianCourseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MianCourseServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ICustomer icust = new CustomerImpl();
		String maincourse=request.getParameter("maincourse");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		
		List<Item> maincourselist = icust.getAllItems("maincourse");
		if (maincourselist.size() == 0) {
			out.println("There is no data");
		} else {
			session.setAttribute("MainCourseList",maincourselist);
		}
		RequestDispatcher rd= request.getRequestDispatcher("maincourse.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
