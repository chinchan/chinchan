package com.training.servlet;

/**
 * @author kkuhik
 *
 */
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.training.bean.Item;
import com.training.service.IItem;
import com.training.service.ItemImpl;

/**
 * Servlet implementation class EditItemServlet
 */
@WebServlet("/editItemServlet")
public class EditItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditItemServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		String item1[]=request.getParameterValues("dish");
		
		Item item = new Item();
		try {for (String items:item1) {
				String itemArray[] = items.split("--");
					item.setItemName(itemArray[0]);
			}
		}
		catch (NullPointerException e) {
			out.print("<h1>Invalid Entries</h1>");
			RequestDispatcher rd = request.getRequestDispatcher("adminitem.jsp");
			rd.include(request, response);
			return;
		}
				// item.setPrice(Double.parseDouble(itemArray[1]));
			
	
		try {
			double price = Double.parseDouble(request.getParameter("price"));
			System.out.println(price);
			if(price<0){
				out.print("Invalid Entries");
				RequestDispatcher rd = request.getRequestDispatcher("adminitem.jsp");
				rd.include(request, response);
			}else
			{
			item.setPrice(price);
			IItem ref = new ItemImpl();
			ref.editItem(item);
			out.println("<h1>Item Edited</h1>");
			
			RequestDispatcher rd = request.getRequestDispatcher("adminitem.jsp");
			rd.include(request, response);
			}
			} catch (NumberFormatException e) {
			response.sendRedirect("adminitem.jsp");
			return;
		}
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
